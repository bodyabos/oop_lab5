﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleClassConlsole
{
    class Product
    {
        protected string name;
        protected float price;
        public Currency cur;
        protected int quantity;
        protected string producer;
        protected float weight;

        public void Main()
        {

        }

        public Product()
        {
            name = "Телефон";
            price = 228;
            quantity = 100;
            producer = "Nokia";
            weight = 1;
            cur = new Currency(cur);
        }
        public Product(string n)
        {
            name = n;
            price = 228;
            quantity = 100;
            producer = "Nokia";
            weight = 1;
            cur = new Currency(cur);
        }


        public Product(string n, float p)
        {
            name = n;
            price = p;
            quantity = 100;
            producer = "Nokia";
            weight = 1;
            cur = new Currency(cur);
        }
        public Product(string n, float p, int q, string na, float w)
        {
            name = n;
            price = p;
            quantity = q;
            producer = na;
            weight = w;
            cur = new Currency(cur);
        }
        public Product(Product previous)
        {
            name = previous.name;
            price = previous.price;
            quantity = previous.quantity;
            producer = previous.producer;
            weight = previous.weight;
            cur = new Currency(cur);
        }

        public string Name
        {
            set;
            get;
        }
        public float Price
        {
            set;
            get;
        }
        public int Quantity
        {
            set;
            get;
        }
        public string Producer
        {
            set;
            get;
        }
        public float Weight
        {
            set;
            get;
        }



        public float GetPriceInUAH()
        {
            return cur.GetExRate() * price;
        }
        public float GetTotalPriceInUAH()
        {
            return cur.GetExRate() * price * quantity;
        }
        public float GetTotalWeight()
        {
            return quantity * weight;
        }
        public int GetQuantity()
        {
            return quantity;
        }
    }
   
    
}
