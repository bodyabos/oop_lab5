﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleClassConlsole
{
    class Currency
    {
        protected  string name;
        protected  float exRate;

        public Currency()
        {
            name = "UAH";
            exRate = 28;
        }
        public Currency(string n, float e)
        {
            name = n;
            exRate = e;
        }
        public Currency(string na)
        {
            name = na;
        }
        public Currency(Currency previous) : this()
        {
            if (previous != null)
            {
                name = previous.name;
                exRate = previous.exRate;
            }
        }

        public float GetExRate()
        {
            return exRate;
        }
        public string GetName()
        {
            return name;
        }
        public string Name
        {
            set;
            get;
        }
        public float ExRate
        {
            set;
            get;
        }
    }
}