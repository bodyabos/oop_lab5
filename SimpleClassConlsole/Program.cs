﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleClassConlsole
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
           System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            Console.Title = "Лабораторна робота №5";
            Console.SetWindowSize(100, 25);
            Console.BackgroundColor = ConsoleColor.White; Console.ForegroundColor = ConsoleColor.DarkBlue; Console.Clear();

            int x = 1;
            Product lower = new Product();
            Product high = new Product();
            while (x != 0)
            {
                Console.WriteLine("\t1. Створити масив товарів\n\t" +
                    "2. Роздрукувати інформацію про один товар\n\t" +
                    "3. Роздрукувати інформацію про всі товари\n\t" +
                    "4. Найдорощий та найдешевший товар\n\t" +
                    "5. Сортування за зростанням ціни\n\t" +
                    "6. Сортування за кількістю товарів на складі\n\t" +

                    "0. Вихід" +
                    "\n-->");

                while (!int.TryParse(Console.ReadLine(), out x))
                {
                    Console.WriteLine("Помилка введення значення. Будь-ласка повторіть введення значення ще раз!");
                }


                switch (x)
                {
                    case 1: { ReadProductsArray(); break; }
                    case 2: { PrintProduct(products[0]); break; }
                    case 3: { PrintProducts(products); break; }
                    case 4: { GetProductsInfo(out lower, out high); break; }
                    case 5: { SortProductsByPrice(); break; }             
                    case 6: { SortProductsByCount(); break; }
                    case 7: {  break; }
                }
            }
            
          
        }

        private static void SortProductsByCount()
        {
            Array.Sort(products, new ProductCountComparer());
        }

        class ProductCountComparer : IComparer<Product>
        {
            public int Compare(Product p1, Product p2)
            {
                if (p1.GetQuantity() > p2.GetQuantity())
                    return 1;
                else if (p1.GetQuantity() < p2.GetQuantity())
                    return -1;
                else
                    return 0;
            }
        }

        class ProductPriceComparer : IComparer<Product>
        {
            public int Compare(Product p1, Product p2)
            {
                if (p1.GetPriceInUAH() > p2.GetPriceInUAH())
                    return 1;
                else if (p1.GetPriceInUAH() < p2.GetPriceInUAH())
                    return -1;
                else
                    return 0;
            }
        }

        private static void SortProductsByPrice()
        {
            Array.Sort(products, new ProductPriceComparer());
        }

        public static int n { get; private set; }

        public static Product[] products = new Product[n];
        public static Currency[] cur = new Currency[n];

        public static Product[] Products { set; get; }

        public static void ReadProductsArray()
        {
            Console.Clear();
            Console.WriteLine("Введіть кількість товарів:");
            if (!byte.TryParse(Console.ReadLine(), out byte n))
            {
                Console.WriteLine("Помилка введення значення. Будь-ласка повторіть введення значення ще раз!");

            }
           
                products = new Product[n];
                cur = new Currency[n];
                for (int i = 0; i < n; i++)
                {
                    products[i] = new Product();
                   
                    Console.WriteLine($"----------------{i}---------------");
                    Console.WriteLine("Введіть ім'я:");
                    products[i].Name = Console.ReadLine();
                    Console.WriteLine("Введіть ціну:");
                    products[i].Price = Convert.ToSingle(Console.ReadLine());
                    Console.WriteLine("Введіть кількість:");
                    products[i].Quantity = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Введіть виробника:");
                    products[i].Producer = Console.ReadLine();
                    Console.WriteLine("Введіть вагу:");
                    products[i].Weight = Convert.ToSingle(Console.ReadLine());



                }
            
        }

        public static void PrintProducts(Product[] products)
        {
            Console.Clear();
            for (int i = 0; i < products.Length; i++)
            {
                Console.WriteLine($"------------------------------------\n" +
                    $"Товар #{i}: {products[i].Name}\n Ціна: {products[i].Price}\n " +
                    $"Кількість: {products[i].Quantity}\n Виробник: {products[i].Producer}\n " +
                    $"Вага: {products[i].Weight}\n");
            }
        }

        public static void PrintProduct(Product product)
        {
            Console.Clear();
            Console.WriteLine($"------------------------------------\n" +
                $"Товар #: {product.Name}\n Ціна: {product.Price}\n " +
                $"Кількість: {product.Quantity}\n Виробник: {product.Producer}\n " +
                $"Вага: {product.Weight}\n");

        }

        public static void GetProductsInfo(out Product lower, out Product high)
        {
            high = products[0];
            lower = products[0];
            float lowCost = products[0].Price, hightCost = products[0].Price;
            for (int i = 0; i < n; i++)
            {
                if (products[i].Price > hightCost) { hightCost = products[i].Price; high = products[i]; }
                if (products[i].Price < lowCost) { lowCost = products[i].Price; lower = products[i]; }
            }
        }

    } 
    
}

